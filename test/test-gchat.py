#! /usr/bin/python3

from google import pubsub_v1
import inspect

# print(dir(pubsub_v1.SubscriberClient))
subscriber = pubsub_v1.SubscriberClient()


project_id = "enter_valid_project_id"
subscription_id = "enter_valid_sub_id"
subscription_path = subscriber.subscription_path(project_id, subscription_id)

# print(inspect.getfullargspec(pubsub_v1.SubscriberClient.pull))

print(inspect.getfullargspec(pubsub_v1.types.PullRequest))
print(dir(pubsub_v1.types.PullRequest))

def pubsub(subscription_path):
    try:
        client = pubsub_v1.SubscriberClient()
        response = client.pull(
            subscription=subscription_path,
            max_messages=1,
            return_immediately=True
        )
        ack_ids = [msg.ack_id for msg in response.received_messages]
        subscriber.modify_ack_deadline(
            request={
                "subscription": subscription_path,
                "ack_ids": ack_ids,
                "ack_deadline_seconds": 0,
            }
        )
    except:
        print("Failed to connect to pubsub")
        raise

pubsub(subscription_path)
