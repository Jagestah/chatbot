# docker login
# docker build -t registry.gitlab.com/jagestah/chatbot:latest . && docker push registry.gitlab.com/jagestah/chatbot:latest

FROM python:3.8
EXPOSE 5000
WORKDIR /src
COPY requirements.txt .
RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt
ENV PYTHONUNBUFFERED=1
COPY ./src .
CMD [ "sleep", "infinity" ]
