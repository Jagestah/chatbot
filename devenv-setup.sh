#! bash

# Install Helm
brew install helm

# Install Helm secrets plugin
brew install gettext

brew install sops

helm plugin install https://github.com/jkroepke/helm-secrets --version v3.10.0

# Install Helm diff plugin
helm plugin install https://github.com/databus23/helm-diff

# Install Microk8s
brew install ubuntu/microk8s/microk8s

microk8s install

# Configure Microk8s to use local docker registry
microk8s enable registry
