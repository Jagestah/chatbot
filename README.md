# Chatbot
## Diagram
![Overview](/docs/Overview.png)

# TODO
- Github Ingester
  - Receive webhooks from Github and run commands from them. IE Jenkins Auto
- HTTP Ingester
  - Receive requests via curl commands for the foundation of CLI scripting
- Postgresql Pod
  - Package in a postgres pod with the Helm Chart
  - Include patterns to leverage the DB
- Task Actioner
  - To create Tasks in Asana or Jira based on a config file that maps a Space or Channel to a tasking platform
  

---
- Create GCP project
- Enable PubSub for GCP project
- Create PubSub Topic
- Enable Chat API according to [The Guide](https://developers.google.com/chat/quickstart/apps-script-bot#step_2_publish_the_bot)
- Let's figure out local dev
- Install KubeMQ [Helm Chart](https://docs.kubemq.io/getting-started/create-cluster/helm) on cluster
- (optional) Install CockroachDB [Helm Chart](https://github.com/cockroachdb/helm-charts) on cluster
- Build Helm Chart for Application
- Add Ingester for Gchat
- Add Responder for Gchat
- Add Action Handler for Asana
- Add Action Router?
- Create diagram

---

## GChat Resources:
- [Chatbot Concepts](https://developers.google.com/chat/concepts/bots)
- [Chatbot Implementation Architecture](https://developers.google.com/chat/concepts/structure)
- [GChat API](https://googleapis.github.io/google-api-python-client/docs/dyn/chat_v1)
- [Creating New Bots](https://developers.google.com/chat/how-tos/bots-develop)
#### Notes
- Responding to a thread requires the SPACE_ID and THREAD_ID [link](https://developers.google.com/chat/how-tos/bots-develop#reply_existing_thread)
- Asynchronous replying requires Service account
- Synchronous replying will have the message in the response
- If creating a new thread a bot can use a [ThreadKey](https://developers.google.com/chat/how-tos/bots-develop#thread_key)
- GChat [Cards](https://developers.google.com/chat/api/guides/message-formats/cards)
- Gchat [Text Message](https://developers.google.com/chat/api/guides/message-formats/basic)
- GChat [Events](https://developers.google.com/chat/api/guides/message-formats/events)
- [Publishing The Bot](https://developers.google.com/chat/how-tos/bots-publish)
