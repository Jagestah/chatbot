#! /usr/bin/python3
import nats
import asyncio
import json
import os
from utils import connection_check, nats_utils

ingress_subject = os.environ["INGRESS_SUBJECT"]
egress_subject = os.environ["EGRESS_SUBJECT"]
nats_server = os.environ["NATS_SERVER"]

async def main():
    options = await nats_utils.get_options(nats_server)
    try:
        nc = await nats.connect(**options)
        js = nc.jetstream()
        print(f"Connected to {nats_server}")
    except Exception as e:
        print(e)
    await nats_utils.create_stream(js, ingress_subject)
    await nats_utils.create_stream(js, egress_subject)

    async def callback(msg):
        headers = msg.headers
        subject = msg.subject
        data = msg.data.decode()
        print(f"Received a message on '{subject}', {headers}: {data}")
        message_egress = f"{egress_subject}"
        task_args = msg.data.decode().split()
        msg_text =  " ".join(task_args[1:])
        print(f"Routing '{msg_text}' to '{message_egress}'")
        ack = await js.publish(
            message_egress,
            msg_text.encode(),
            headers=msg.headers
        )
        print(ack)

    print(f"Subscribing to {ingress_subject}")
    await js.subscribe(
        ingress_subject,
        ingress_subject,
        cb=callback
    )
    

connection_check.nats(nats_server)
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    try:
        loop.run_forever()
    finally:
        loop.close()
