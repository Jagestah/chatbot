#! /usr/bin/python3
import nats
from utils import connection_check, gchat_utils, nats_utils
import asyncio
import os
import json

ingress_subject = os.environ["INGRESS_SUBJECT"]
egress_subject = os.environ["EGRESS_SUBJECT"]
nats_server = os.environ["NATS_SERVER"]

group_dict = {
    # '<group1-name>': [
    #     'users/<user1-id>',
    #     'users/<user2-id>'
    # ],
    # '<group2-name>': [
    #     'users/<user1-id>',
    #     'users/<user2-id>'
    # ]
}
print(f'configured with {group_dict}')

async def main():
    options = await nats_utils.get_options(nats_server)
    try:
        nc = await nats.connect(**options)
        js = nc.jetstream()
        print(f"Connected to {nats_server}")
    except Exception as e:
        print(e)
    await nats_utils.create_stream(js, ingress_subject)
    await nats_utils.create_stream(js, egress_subject)

    async def callback(msg):
        headers = msg.headers
        subject = msg.subject
        data = msg.data.decode()
        print(f"Received a message on '{subject}', {headers}: {data}")
        output = ''
        task_args = await argument_parse(msg.data.decode())
        if not task_args: #  If task_args is empty
            output = await group_output(output)
            print(f"responding with {output}")
            await js.publish(
                egress_subject,
                output.encode(),
                headers=msg.headers
            )  # Respond with currently configured groups
            return
        else:
            group_name = task_args[0].lower()
        if group_name in group_dict:
            output = await user_output(group_name)
            print(f"responding with {output}")
            await js.publish(
                egress_subject,
                output.encode(),
                headers=msg.headers
            )  # Respond with mentions of users in the desired group
        else:
            await js.publish(
                egress_subject,
                f'No group found for `{group_name}`'.encode(),
                headers=msg.headers
            )  # Respond with error
        return

    async def group_output(output):
        group_list = list(group_dict.keys())
        output = f'Configured groups: `{"` `".join(group_list)}`'
        return output

    async def user_output(group_name):
        user_list = list(group_dict[group_name])
        output = f'<{"> <".join(user_list)}>'
        return output


    async def argument_parse(args):
        # print("parsing arguments")
        # Verify
        # split on whitespace
        task_args = args.split()
        # return task_args
        return task_args[1:]

    print(f"Subscribing to {ingress_subject}")
    await js.subscribe(
        ingress_subject,
        ingress_subject,
        cb=callback
    )
    

connection_check.nats(nats_server)
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    try:
        loop.run_forever()
    finally:
        loop.close()
