#! /usr/bin/python3
import nats
import asyncio
import json
import os
from utils import connection_check, nats_utils

ingress_subject = os.environ["INGRESS_SUBJECT"]
egress_subject = os.environ["EGRESS_SUBJECT"]
nats_server = os.environ["NATS_SERVER"]

async def main():
    options = await nats_utils.get_options(nats_server)
    try:
        nc = await nats.connect(**options)
        js = nc.jetstream()
        print(f"Connected to {nats_server}")
    except Exception as e:
        print(e)
    await nats_utils.create_stream(js, ingress_subject)

    async def callback(msg):
        headers = msg.headers
        subject = msg.subject
        data = msg.data.decode()
        print(f"Received a message on '{subject}', {headers}: {data}")
        message_egress = f"{egress_subject}{msg.headers['network']}"
        print(f"Routing '{data}' to '{message_egress}'")
        await nc.publish(
            message_egress,
            msg.data,
            headers=msg.headers
        )

    print(f"Subscribing to {ingress_subject}")
    await js.subscribe(
        ingress_subject,
        ingress_subject,
        cb=callback
    )

connection_check.nats(nats_server)
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    try:
        loop.run_forever()
    finally:
        loop.close()
