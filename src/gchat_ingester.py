#! /usr/bin/python3

from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1
import pprint
import json
import os
import asyncio
import nats
from nats.errors import ConnectionClosedError, TimeoutError, NoServersError
from utils import connection_check, nats_utils

# headers = [
#     ("network", "gchat"),
#     ("channel", "message.annotations.space.name")
#     ("team", ????),
#     ("user", "message.annotations.sender.email")
#     ("thread", "message.annotations.thread.name")
#     ("channel_type", "message.annotations.space.type")
# ]

pp = pprint.PrettyPrinter(indent=2)
project_id = os.environ["GCHAT_PROJECT_ID"]
subscription_id = os.environ["GCHAT_SUB_ID"]
nats_server = os.environ["NATS_SERVER"]
egress_subject = os.environ["EGRESS_SUBJECT"]
# Number of seconds the subscriber should listen for messages
timeout = 5.0
subscriber = pubsub_v1.SubscriberClient()
subscription_path = subscriber.subscription_path(project_id, subscription_id)


async def main():
    def callback(message: pubsub_v1.subscriber.message.Message) -> None:
        print(f"Received {message.data}.")
        message_dict = json.loads(message.data)
        try:
            if message_dict["type"] == "ADDED_TO_SPACE":
                if 'displayName' in message_dict['space']:
                    print(f"Added to channel: {message_dict['space']['displayName']}")
                else:
                    print(f"Added to DM with {message_dict['user']['displayName']}")
            elif message_dict["type"] == "REMOVED_FROM_SPACE":
                if 'displayName' in message_dict['space']:
                    print(f"Removed from channel: {message_dict['space']['displayName']}")
                else:
                    print(f"Removed from DM with {message_dict['user']['displayName']}")
                pass
            else:
                asyncio.run(send_to_queue(message_dict))
            message.ack()
        except:
            message.nack()
            raise

    async def assign_headers(message_dict):
        headers = {
            'network': 'gchat',
            'original': json.dumps(message_dict),
            'channel': message_dict["message"]["space"]["name"],
            'user': message_dict["message"]["sender"]["email"],
            'thread': message_dict["message"]["thread"]["name"],
            'channel_type': message_dict["message"]["space"]["type"]
        }
        return headers

    async def respond_to_add(message_dict):
        nc = NATS()
        await nc.connect(servers=[f"nats://{nats_server}:4222"])
        headers = assign_headers(message_dict)
        try:
            nc = await nats.connect(servers=[f"nats://{nats_server}:4222"])
            js = nc.jetstream()
            print(f"Connected to {nats_server}")
            await nats_utils.create_stream(js, egress_subject)
        except Exception as e:
            print(e)
        headers = await assign_headers(message_dict)
        try:
            await js.publish(
                "router_response",
                "Thanks for adding me to this Space!",
                headers=headers
            )
        except:
            print(Exception)
            raise
        await nc.close()

    async def send_to_queue(message_dict):
        options = await nats_utils.get_options(nats_server)
        try:
            nc = await nats.connect(servers=[f"nats://{nats_server}:4222"])
            js = nc.jetstream()
            print(f"Connected to {nats_server}")
            await nats_utils.create_stream(js, egress_subject)
        except Exception as e:
            print(e)
        await nats_utils.create_stream(js, egress_subject)
        headers = await assign_headers(message_dict)
        if 'argumentText' in message_dict["message"]:
            message_text = message_dict["message"]["argumentText"].strip().encode()
        else:
            print("Empty message: no messageText")
            return
        print(f"Sending '{message_text.decode()}' to '{egress_subject}'")
        try:
            await js.publish(
                egress_subject,
                message_text,
                headers=headers
            )
        except:
            print(Exception)
            raise
        await nc.close()

    print(f"Listening for GChat messages on {subscription_path}..\n")
    print(f"Publishing messages to {egress_subject} on {nats_server}")
    streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)

    # Wrap subscriber in a 'with' block to automatically call close() when done.
    with subscriber:
        try:
            # When `timeout` is not set, result() will block indefinitely,
            # unless an exception is encountered first.
            streaming_pull_future.result()
        except TimeoutError:
            streaming_pull_future.cancel()  # Trigger the shutdown.
            streaming_pull_future.result()  # Block until the shutdown is complete.

connection_check.pubsub(subscription_path)
connection_check.nats(nats_server)
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    try:
        loop.run_forever()
    finally:
        loop.close()
