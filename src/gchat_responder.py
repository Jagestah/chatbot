#! /usr/bin/python3

from googleapiclient.discovery import build
import nats
import asyncio
import google.auth
import json
import os
import random
import string
from utils import connection_check, nats_utils

scopes = ['https://www.googleapis.com/auth/chat.bot']
credentials, project_id = google.auth.default()
credentials = credentials.with_scopes(scopes=scopes)
chat = build('chat', 'v1', credentials=credentials)

nats_server = os.environ["NATS_SERVER"]
ingress_subject = os.environ["INGRESS_SUBJECT"]

async def main():
    options = await nats_utils.get_options(nats_server)
    try:
        nc = await nats.connect(**options)
        js = nc.jetstream()
        print(f"Connected to {nats_server}")
    except Exception as e:
        print(e)
    await nats_utils.create_stream(js, ingress_subject)

    async def callback(msg):
        headers = msg.headers
        subject = msg.subject
        data = msg.data.decode()
        print(f"Received a message on '{subject}', {headers}: {data}")
        thread_key = None
        response_dict = {
            "thread": {
                "name": msg.header["thread"]
            },
            "text": msg.data.decode()
        }
        if msg.header["thread"] == 'new':
            new_thread_id = ''.join(random.choices(string.ascii_lowercase, k=8))
            thread_key = new_thread_id
            print(f"Creating new thread with ID: {new_thread_id}")
            response_dict["thread"]["name"] = ""
        try:
            chat.spaces().messages().create(
                parent=msg.headers["channel"],
                threadKey=thread_key,
                body=response_dict
            ).execute()
        except:
            print(Exception)
            await msg.ack()
            raise
        await msg.ack()

        print(f"Sent '{data}' to channel '{msg.headers['channel']}'")

    print(f"Subscribing to {ingress_subject}")
    await js.subscribe(
        ingress_subject,
        ingress_subject,
        cb=callback
    )
    

connection_check.nats(nats_server)
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    try:
        loop.run_forever()
    finally:
        loop.close()
