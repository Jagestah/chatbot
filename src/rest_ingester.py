#! /usr/bin/python3

from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1
import pprint
import json
import os
import asyncio
import nats
from nats.errors import ConnectionClosedError, TimeoutError, NoServersError
from utils import connection_check, nats_utils

import threading
import asyncio
from flask import Flask, jsonify, request, Response
import uuid

nats_server = os.environ["NATS_SERVER"]
egress_subject = os.environ["EGRESS_SUBJECT"]
ingress_subject = os.environ["INGRESS_SUBJECT"]


# headers = [
#     ("network", "rest"),
#     ("channel", ????), # assign UUID
#     ("team", ????),
#     ("user", message.email),
#     ("thread", "message.annotations.thread.name")
#     ("channel_type", "message.annotations.space.type")
# ]

print(f"In flask global level: {threading.current_thread().name}")
app = Flask(__name__)

@app.route("/", methods=["POST"])
async def index():
    validated_message = await validate_body(request)
    message_body = validated_message['message']
    if validated_message['valid']:
        # Build the message
        message_uuid = str(uuid.uuid4())
        message_headers, message_text = await build_message(message_body, message_uuid)
        # Send off the message to the action router
        print(message_headers)
        response = await message_loop(message_headers, message_text, message_uuid)


        print(f"Sending response: '{response}'")
        return Response(response, status=200)
    else:
        return Response(validated_message['message'], status=validated_message['status_code'])
        
async def message_loop(message_headers, message_text, message_uuid):
    options = await nats_utils.get_options(nats_server)
    try:
        nc = await nats.connect(**options)
        js = nc.jetstream()
        print(f"Connected to {nats_server}")
    except Exception as e:
        print(e)
    await nats_utils.create_stream(js, ingress_subject)
    await nats_utils.create_stream(js, egress_subject)

    async def callback(msg):
        print("Got a message")
        if message_uuid in msg.header["channel"]:
            # Continue processing the message
            print(f"Message matches {message_uuid}")
            headers = msg.headers
            subject = msg.subject
            data = msg.data.decode()
            future.set_result(data)
        else:
            # Go back to listening
            print(f"Message didn't match {message_uuid}")
            await js.subscribe(ingress_subject, cb=callback)
    
    print(f"Sending '{message_text.decode()}' to '{egress_subject}'")
    try:
        await js.subscribe(ingress_subject, ingress_subject, cb=callback)
        ack = await js.publish(
            egress_subject,
            message_text,
            headers=message_headers
        )
        print(ack)
    except:
        print(Exception)
        raise
    if message_headers['channel'] != message_uuid:
        return f"Message sent to {message_headers['channel']}"
    # Listen for the response the matches
    future = asyncio.Future()
    print(f"Listening for message: {message_uuid}")
    data = await asyncio.wait_for(future, 30)
    await nc.close()
    return data

async def build_message(message, message_uuid):
    if 'network' in message:
        network = message['network']
    else:
        network = 'rest'
    if 'channel' in message:
        channel = message['channel']
    else:
        channel = message_uuid
    if 'thread' in message:
        thread = message['thread']
    else:
        thread = 'Not Implemented'
    if 'channel_type' in message:
        channel_type = message['channel_type']
    else:
        channel_type = 'DM'
    message_headers = {
        'network': network,
        'original': json.dumps(message),
        'channel': channel,
        'user': message["email"],
        'thread': thread,
        'channel_type': channel_type
    }
    message_text = message["message"].strip().encode()
    return message_headers, message_text

async def validate_body(request):
    print("Validating request")
    request_dict = {}
    if request.get_json():
        print("Request is JSON")
        if "email" in request.get_json():
            # print("email found in JSON")
            # print(request.get_json()['email'])
            # print(request.get_json()['message'])
            request_dict = {
                'valid':True,
                'status_code':200,
                'message': request.get_json()
            }
            return request_dict
        else:
            # print("Email not found in request")
            request_dict = {
                'valid':False,
                'status_code':400,
                'message': 'email not present in JSON'
            }
            return request_dict
    else:
        print("Request wasn't JSON")
        request_dict = {
            'valid':False,
            'status_code':400,
            'message': 'Message not JSON'
        }
        return request_dict


connection_check.nats(nats_server)
if __name__ == "__main__":
    app.run(host="0.0.0.0", port="80")

# curl -X POST localhost:8080/ -H "Content-Type: application/json" -d '{"message": "parrot Now the parrot function can be used to send arbitrary text to gchat","channel": "spaces/<space-id>","thread": "spaces/<space-id>/threads/<thread-id>","network": "gchat"}'