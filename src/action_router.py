#! /usr/bin/python3
import nats
import asyncio
import json
import os
from utils import connection_check, nats_utils

actioners = [
    "parrot",
    "task",
    "lookup",
    "ping",
]

ingress_subject = os.environ["INGRESS_SUBJECT"]
egress_subject = os.environ["EGRESS_SUBJECT"]
nats_server = os.environ["NATS_SERVER"]

async def main():
    options = await nats_utils.get_options(nats_server)
    try:
        nc = await nats.connect(**options)
        js = nc.jetstream()
        print(f"Connected to {nats_server}")
    except Exception as e:
        print(e)
    await nats_utils.create_stream(js, ingress_subject)
    for actioner in actioners:
        await nats_utils.create_stream(js, f"{egress_subject}{actioner}")

    async def callback(msg):
        headers = msg.headers
        subject = msg.subject
        data = msg.data.decode()
        action_target = data.split()[0]
        print(f"Received a message on '{subject}', {headers}: {data}")
        if action_target in actioners:
            message_egress = f"{egress_subject}{action_target}"
            ack = await js.publish(
                message_egress,
                msg.data,
                headers=msg.headers
            )
            print(ack)
        else:
            txt = f"`{action_target}` didn't match known actioners"
            print(txt)
            ack = await js.publish(
                "router_response",
                txt.encode(),
                headers=msg.headers
            )
            print(ack)

    print(f"Subscribing to {ingress_subject}")
    await js.subscribe(
        ingress_subject,
        ingress_subject,
        cb=callback
    )
    

connection_check.nats(nats_server)
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    try:
        loop.run_forever()
    finally:
        loop.close()
