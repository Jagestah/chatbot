#! /usr/bin/python3

import psycopg2
import os

db_username = os.environ['DB_USERNAME']
db_host = os.environ['DB_HOST']
db_port = os.environ['DB_PORT']
db_database = os.environ['DB_DATABASE']

# Open a cursor to perform database operations
print(f"connected to {db_host}")


def db_connect():
    conn = psycopg2.connect(
        host=db_host,
        port=db_port,
        database=db_database,
        sslmode='disable'
    )
    cur = conn.cursor()
    return cur, conn


def db_disconnect(cur, conn):
    cur.close()
    conn.close()
    return


def db_insert(sql, data):
    cur, conn = db_connect()
    cur.execute(sql, data)
    print(f"ran sql cmd: {sql}")
    print(f"with data: {data}")
    rowcount = cur.rowcount
    print(f"Affected {rowcount} rows")
    conn.commit()
    db_disconnect(cur, conn)
    return rowcount


def db_execute(sql):
    cur, conn = db_connect()
    cur.execute(sql)
    print(f"ran sql cmd: {sql}")
    rowcount = cur.rowcount
    print(f"Affected {rowcount} rows")
    conn.commit()
    db_disconnect(cur, conn)
    return rowcount


def db_lookup(sql):
    cur, conn = db_connect()
    cur.execute(sql)
    print(f"ran sql cmd: {sql}")
    results = cur.fetchall()
    rowcount = cur.rowcount
    print(f"Affected {rowcount} rows")
    conn.commit()
    db_disconnect(cur, conn)
    return results


# Connect to an existing database

db_execute("CREATE TABLE IF NOT EXISTS asana_tasks (task_gid varchar(256) PRIMARY KEY, project_gid varchar(256), channel varchar(256), thread varchar(256));")


# db_insert(insert_sql, insert_data)
# results = db_lookup(lookup_sql)
# print(results)
# db_execute(cleanup_sql)
