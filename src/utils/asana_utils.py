#! /usr/bin/python3
# import sys
import os
import json
import asana
from utils import postgres_utils

asana_token = os.environ['ASANA_TOKEN']
workspace = os.environ['ASANA_WORKSPACE']
bot_name = os.environ['BOT_NAME']
bot_tag = os.environ['BOT_TAG']
# project = os.environ["ASANA_PROJECT"]

# Create task example: https://github.com/Asana/python-asana/blob/master/examples/example-create-task.py

client = asana.Client.access_token(asana_token)

# Verify the creds
me = client.users.me()

async def user_lookup(email):
    result = client.users.get_users({'workspace': workspace}, fields="email,name")
    found = False
    for user in result:
        if user['email'] == email:
            found = True
            print(user)
            return user
            break
    if not found:
        print("User not found.")
        return


async def create_task(msg, task_name, project_gid, thread_link, task_description):
    print("creating task")
    query_dict = {}
    user = await user_lookup(msg.headers["user"])
    task = client.tasks.create_in_workspace(
        workspace,
        {
            'name': task_name,
            'html_notes': f'<body>Created by {bot_name} on behalf of <a data-asana-gid="{user["gid"]}"/>\n<a href="{thread_link}">Link to the {msg.headers["network"]} context </a></body>',
            'projects': [project_gid],
            'tags': [bot_tag]
        }
    )
    # Create DB entry
    query_dict['task_gid'] = task["gid"]
    query_dict['project_gid'] = project_gid
    query_dict['channel'] = msg.headers['thread'].split("/")[1]
    query_dict['thread'] = msg.headers['thread'].split("/")[3]
    insert_sql = "INSERT INTO asana_tasks VALUES (%(task_gid)s, %(project_gid)s, %(channel)s, %(thread)s)"
    postgres_utils.db_insert(insert_sql, query_dict)
    # print(json.dumps(task, indent=2))
    print(f"Created task {task['gid']}")

    return task['permalink_url']


async def db_task_lookup(msg):
    # Look up whether the channel/thread combo exists in the DB
    # Return the contents of the task column
    # task_found = False # Replace this with a db call
    thread = msg.headers['thread'].split("/")[3]
    sql = f"SELECT task_gid FROM asana_tasks WHERE thread='{thread}'"
    task_tuple = postgres_utils.db_lookup(sql)
    task_gid = ''
    if task_tuple:
        task_gid = task_tuple[0][0]
        task_tuple = tuple()
        print(f"found this while looking up the task: {task_gid}")
    if task_gid:
        return task_gid
    else:
        return ''


async def assign_task(msg, task_gid, email):
    user = await user_lookup(email)
    result = client.tasks.update_task(task_gid, {'assignee': user["gid"]}, opt_pretty=True)
    # print(json.dumps(result, indent=2))
    return


async def complete_task(msg, task_gid):
    # print("Looking up user")
    user = await user_lookup(msg.headers["user"])
    # print("Creating the story")
    client.stories.create_story_for_task(
            task_gid,
            {
                'html_text': f'<body>Completed by {bot_name} on behalf of <a data-asana-gid="{user["gid"]}"/></body>'
            },
            opt_pretty=True
        )
    # print(f"Marking task {task_gid} as complete")
    client.tasks.update_task(task_gid, {'completed': 'true'})
    task_dict = client.tasks.get_task(task_gid, opt_pretty=True)
    task_project_gid = task_dict['memberships'][0]['project']['gid']
    project_sections = client.sections.get_sections_for_project(task_project_gid, {'name': 'Done'}, opt_pretty=True)
    done_section_gid = "none"
    for section in project_sections:
        if section['name'] == 'Done':
            done_section_gid = section['gid']
    client.tasks.add_project_for_task(task_gid, {'project': task_project_gid,'section': done_section_gid}, opt_pretty=True)
    # print("Task marked as complete")
    return

async def open_task(msg, task_gid):
    user = await user_lookup(msg.headers["user"])
    result = client.stories.create_story_for_task(
            task_gid,
            {
                'html_text': f'<body>Reopened by {bot_name} on behalf of <a data-asana-gid="{user["gid"]}"/></body>'
            },
            opt_pretty=True
        )
    result = client.tasks.update_task(task_gid, {'completed': 'false'}, opt_pretty=True)
    return
