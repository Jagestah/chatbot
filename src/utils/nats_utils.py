#! /usr/bin/python3
import nats
import asyncio

async def error_cb(e):
        print("Error:", e)

async def closed_cb():
    # Wait for tasks to stop otherwise get a warning.
    await asyncio.sleep(0.2)
    loop.stop()

async def reconnected_cb():
    print(f"Connected to NATS at {nc.connected_url.netloc}...")

async def get_options(nats_server):
    options = {
        "error_cb": error_cb,
        "closed_cb": closed_cb,
        "reconnected_cb": reconnected_cb,
        "servers": [f"nats://{nats_server}:4222"]
    }
    return options

async def create_stream(js, subject):
    print(f"Creating Stream {subject}")
    await js.add_stream(
        name=subject,
        subjects=[subject]
    )
