#! /usr/bin/python3

from nats.aio.client import Client as NATS
import asyncio
import google.auth
import json
import os
from google.cloud import pubsub_v1

def parse_mentions(msg):
    return

async def parse_mentioned_users(msg):
    mentioned_user_list = []
    humans = []
    sender = {}
    all_mentions = []
    if 'original' in msg.headers:
        original_message = json.loads(msg.headers['original'])
        if 'annotations' in original_message['message']:
            annotations = original_message['message']['annotations']
            sender = original_message['message']['sender']
            for annotation in annotations:
                if 'userMention' in annotation and annotation['type'] == 'USER_MENTION':
                    if annotation['userMention']['user']['type'] == 'HUMAN':
                        # print(f'confirmed {annotation["userMention"]["user"]["name"]} as human')
                        humans.append(annotation['userMention']['user'])
                all_mentions.append(annotation['userMention']['user'])
    return(sender, humans, all_mentions)

async def argument_parse(args):
        # print("parsing arguments")
        # Verify
        # split on whitespace
        task_args = args.split()
        # return task_args
        return task_args[1:]