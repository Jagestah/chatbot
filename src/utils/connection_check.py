#! /usr/bin/python3

from nats.aio.client import Client as NATS
import asyncio
import google.auth
import json
import os
from google.cloud import pubsub_v1


def nats(nats_server):
    # Connect to NATS
    try:
        nc = NATS()
        nc.connect(f"nats://{nats_server}:4222")
        # Touch healthcheck file
        file = open('/app/healthcheck.txt', 'w+')
        nc.close()
        print(f"Successfully connected to NATS server {nats_server}")
    except:
        print(f"Failed to connect to NATS server {nats_server}")
        raise


def pubsub(subscription_path):
    try:
        client = pubsub_v1.SubscriberClient()
        response = client.pull(
            subscription=subscription_path,
            max_messages=1,
            return_immediately=True
        )
        ack_ids = [msg.ack_id for msg in response.received_messages]
        if ack_ids:
            client.modify_ack_deadline(
                subscription=subscription_path,
                ack_ids=ack_ids,
                ack_deadline_seconds=0
            )
        print(f"Successfully connected to pubsub {subscription_path}")
    except:
        print(f"Failed to connect to pubsub {subscription_path}")
        raise
