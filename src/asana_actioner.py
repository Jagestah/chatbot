#! /usr/bin/python3
import nats
from utils import connection_check, asana_utils, postgres_utils, gchat_utils, nats_utils
import asyncio
import os

ingress_subject = os.environ["INGRESS_SUBJECT"]
egress_subject = os.environ["EGRESS_SUBJECT"]
nats_server = os.environ["NATS_SERVER"]
project_gid = os.environ["ASANA_PROJECT"]
workspace = os.environ['ASANA_WORKSPACE']

# invoker_email = "garble@example.com"
# task_name = "Do a thing"
# task_gid = "120212354654878"
# thread_link = "this is where I'd put a link"

space_board_mappings = {
    "spaces/<space-id>": "<asana-board-id>"
}

async def main():
    options = await nats_utils.get_options(nats_server)
    try:
        nc = await nats.connect(**options)
        js = nc.jetstream()
        print(f"Connected to {nats_server}")
    except Exception as e:
        print(e)
    await nats_utils.create_stream(js, ingress_subject)
    await nats_utils.create_stream(js, egress_subject)

    async def callback(msg):
        headers = msg.headers
        subject = msg.subject
        data = msg.data.decode()
        print(f"Received a message on {subject}: {data}")
        if msg.headers["channel"] not in space_board_mappings:
            print(f"Unable to find {msg.headers['channel']} in space_board_mappings")
            ack = await js.publish(
                egress_subject,
                f"Bot not configured for this channel: {msg.headers['channel']}".encode(),
                headers=msg.headers
            )
            return
        else:
            project_gid = space_board_mappings[msg.headers["channel"]]
        task_title = msg.data.decode().split("\n")
        task_args = await gchat_utils.argument_parse(task_title[0])
        task_gid = await asana_utils.db_task_lookup(msg)

        if task_gid:
            # print(f"Found task entry in DB: {task_gid}")
            # If there's an Asana task for this channel/thread combo
            if not task_args:
                # If task_args is an empty string
                url = await asana_url_builder(project_gid, task_gid)
                print(f"Responding with task link to {project_gid}/{task_gid}")
                ack = await js.publish(
                    egress_subject,
                    f"Found task: {url}".encode(),
                    headers=msg.headers
                )  # respond with link to task and argument options
            if task_args[0] == 'assign':
                sender, humans, all_mentions = await gchat_utils.parse_mentioned_users(msg)
                # print(humans)
                if not humans:  # Assign the task to the sender
                    assign_user = sender
                elif len(humans) > 2:
                    ack = await js.publish(
                        egress_subject,
                        "Only mention a single user to assign a task to".encode(),
                        headers=msg.headers
                    )  # respond that we're assigning the task
                else:
                    assign_user = humans[0]
                await asana_utils.assign_task(msg, task_gid, assign_user['email'])  # Assign the task to the referenced user
                print(f"Assigning task {project_gid}/{task_gid} to {assign_user}")
                ack = await js.publish(
                    egress_subject,
                    f"Assigned task to <{assign_user['name']}>".encode(),
                    headers=msg.headers
                )  # respond that we're assigning the task
            elif task_args[0] in ['close','done']:
                await asana_utils.complete_task(msg, task_gid)  # Mark the task as complete
                url = await asana_url_builder(project_gid, task_gid)
                ack = await js.publish(
                    egress_subject,
                    f"Closed task: {url}".encode(),
                    headers=msg.headers
                )  # respond that the task was closed
            elif task_args[0] == 'open':
                await asana_utils.open_task(msg, task_gid)  # Open the task
                url = await asana_url_builder(project_gid, task_gid)

                print(f"Opening task {project_gid}/{task_gid}")
                ack = await js.publish(
                    egress_subject,
                    f"Reopened task: {url}".encode(),
                    headers=msg.headers
                )  # respond that the task was closed
            else:
                ack = "No match on task command"
        else:
            print("Did not find task entry in DB")
            if task_args:
                # If there isn't an Asana task for this channel/thread combo
                thread_parse = msg.headers["thread"].split("/")
                thread_path = f"{thread_parse[1]}/{thread_parse[3]}"
                thread_link = f'https://chat.google.com/room/{thread_path}'
                task_name = " ".join(task_args)
                task_description = "\n".join(task_title[1:])
                task_url = await asana_utils.create_task(msg, task_name, project_gid, thread_link, task_description)  # Create Asana Task
                # Create DB entry
                ack = await js.publish(
                    egress_subject,
                    f"Created task: {task_url}".encode(),
                    headers=msg.headers
                )  # respond with link to task and tag invoker
            else:
                ack = await js.publish(
                    egress_subject,
                    f"Please provide a title for the desired task.".encode(),
                    headers=msg.headers
                )  # respond with link to task and tag invoker

        print(ack)
        return

    async def asana_url_builder(project_gid, task_gid):
        url = f"https://app.asana.com/0/{project_gid}/{task_gid}"
        return url

    async def argument_sanity_check(task_args):
        # Check task_args for compliance with Asana's assumptions
        # If it fails, respond back in the thread that it failed.
        return

    print(f"Subscribing to {ingress_subject}")
    await js.subscribe(
        ingress_subject,
        ingress_subject,
        cb=callback
    )
    

connection_check.nats(nats_server)
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    try:
        loop.run_forever()
    finally:
        loop.close()
