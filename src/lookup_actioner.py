#! /usr/bin/python3
import nats
from utils import connection_check, gchat_utils, nats_utils
import asyncio
import os
import json

ingress_subject = os.environ["INGRESS_SUBJECT"]
egress_subject = os.environ["EGRESS_SUBJECT"]
nats_server = os.environ["NATS_SERVER"]

async def main():
    options = await nats_utils.get_options(nats_server)
    try:
        nc = await nats.connect(**options)
        js = nc.jetstream()
        print(f"Connected to {nats_server}")
    except Exception as e:
        print(e)
    await nats_utils.create_stream(js, ingress_subject)
    await nats_utils.create_stream(js, egress_subject)

    async def callback(msg):
        headers = msg.headers
        subject = msg.subject
        data = msg.data.decode()
        print(f"Received a message on '{subject}', {headers}: {data}")
        # print(msg)
        mentioned_user_list = []
        thread_parse = msg.headers["thread"].split("/")
        thread_path = f"{thread_parse[1]}/{thread_parse[3]}"
        thread_link = f'https://chat.google.com/room/{thread_path}'
        chat_info = (
            f"Thread link: {thread_link}\n"
            f"Channel: `{thread_parse[1]}`\n"
            f"Thread: `{thread_parse[3]}`"
        )
        await js.publish(
                egress_subject,
                chat_info.encode(),
                headers=msg.headers
            )  # respond with info about the room and thread
        task_args = await gchat_utils.argument_parse(msg.data.decode())
        sender, humans, all_mentions = await gchat_utils.parse_mentioned_users(msg)

        if 'self' in task_args:
            print("Found self in args")
            if sender:
                mentioned_user_list.append(sender)
        if not humans:
            if not mentioned_user_list:
                print("No humans mentioned")
                return
        print(f'mentioned users:{mentioned_user_list}')
        for human in humans:
            mentioned_user_list.append(human)
        for mention in mentioned_user_list:
            output = await output_format(mention)
            await nc.publish(
                egress_subject,
                output.encode(),
                headers=msg.headers
            )  # respond with info about the mentioned users
        return

    async def output_format(mention):
        print(mention)
        if mention:
            display_name = mention['displayName']
            gchat_name = mention['name']
            email = mention['email']
            return (
                f'*{display_name}*\n'
                f'  GChat: `{gchat_name}`\n'
                f'  Email: `{email}`'
            )
        else:
            return

    print(f"Subscribing to {ingress_subject}")
    await js.subscribe(
        ingress_subject,
        ingress_subject,
        cb=callback
    )


connection_check.nats(nats_server)
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    try:
        loop.run_forever()
    finally:
        loop.close()
